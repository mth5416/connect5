/**
 * Board.java
 * Creates and runs a game of Connect 4
 *
 * @author Mason Hicks
 */

//Imports
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Board extends Application {

    //Paths for images
    private final String PATH_RED = "file:/Users/mason/Desktop/CMPSC221/Connect4/Resources/RedChip.png";
    private final String PATH_BLACK = "file:/Users/mason/Desktop/CMPSC221/Connect4/Resources/BlackChip.png";
    private final String PATH_EMPTY = "file:/Users/mason/Desktop/CMPSC221/Connect4/Resources/EmptyChip.png";
    private final String PATH_TITLE = "file:/Users/mason/Desktop/CMPSC221/Connect4/Resources/title.png";

    //Constants
    private final Image RED = new Image(PATH_RED);
    private final Image BLACK = new Image(PATH_BLACK);
    private final Image EMPTY = new Image(PATH_EMPTY);
    private final Image TITLE = new Image(PATH_TITLE);

    private final int ROWS = 6;
    private final int COLUMNS = 7;

    //Instance variables
    private Color[][] boardColor;
    private ImageView[][] boardImage;
    private Color nextTurn;
    private boolean winner;
    private ImageView turn;
    private Label current;

    private Scene boardScene;
    private Scene titleScene;

    private Stage trueStage;

    /**
     * Used by start() to initialize arrays
     * @param image ImageView[][] array for images
     * @param color Color[][] array for colors
     */
    private void initializeBoard(ImageView[][] image, Color[][] color){
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLUMNS; j++){
                image[i][j] = new ImageView(EMPTY);
                color[i][j] = Color.EMPTY;
            }
        }
        winner = false;
    }

    /**
     * Starts the application
     *
     * @param primaryStage Stage
     * @throws Exception idk what this does
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        //Basic variables
        nextTurn = Color.RED;

        //Sets up board
        boardColor = new Color[ROWS][COLUMNS];
        boardImage = new ImageView[ROWS][COLUMNS];
        initializeBoard(boardImage, boardColor);

        trueStage = primaryStage;

        ImageView titleImage = new ImageView(TITLE);
        Button start = new Button("Start");
        start.setMinSize(128,64);
        VBox buttonHolder = new VBox(start);
        buttonHolder.setPadding(new Insets(32));
        buttonHolder.setAlignment(Pos.CENTER);
        start.setOnAction(new StartHandler());
        VBox startBox = new VBox(titleImage, buttonHolder);
        startBox.setAlignment(Pos.CENTER);
        startBox.setSpacing(20);
        titleScene = new Scene(startBox);

        //Sets up buttons
        Button[] columnButtons = new Button[COLUMNS];
        for(int i = 0; i < COLUMNS; i++){
            columnButtons[i] = new Button();
            columnButtons[i].setMinSize(64,16);
        }

        columnButtons[0].setOnAction(new Column0Handler());
        columnButtons[1].setOnAction(new Column1Handler());
        columnButtons[2].setOnAction(new Column2Handler());
        columnButtons[3].setOnAction(new Column3Handler());
        columnButtons[4].setOnAction(new Column4Handler());
        columnButtons[5].setOnAction(new Column5Handler());
        columnButtons[6].setOnAction(new Column6Handler());

        HBox buttonRow = new HBox(columnButtons[0],
                                  columnButtons[1],
                                  columnButtons[2],
                                  columnButtons[3],
                                  columnButtons[4],
                                  columnButtons[5],
                                  columnButtons[6]);

        //Uses VBox and HBox to setup visual board
        VBox[] columnImages = new VBox[COLUMNS];
        for(int i = 0; i < COLUMNS; i++) {
            columnImages[i] = new VBox(boardImage[0][i],
                                       boardImage[1][i],
                                       boardImage[2][i],
                                       boardImage[3][i],
                                       boardImage[4][i],
                                       boardImage[5][i]);
        }

        HBox combinedImages = new HBox(columnImages[0],
                                       columnImages[1],
                                       columnImages[2],
                                       columnImages[3],
                                       columnImages[4],
                                       columnImages[5],
                                       columnImages[6]);

        VBox boardWithButtons = new VBox(buttonRow, combinedImages);
        boardWithButtons.setPadding(new Insets(10));

        //Stuff for side panel
        current = new Label("Current");
        turn = new ImageView(getImage(nextTurn));
        Button reset = new Button("Reset");

        reset.setOnAction(new ResetHandler());

        VBox sidePanel = new VBox(current,turn,reset);
        sidePanel.setAlignment(Pos.CENTER);
        sidePanel.setSpacing(20);
        sidePanel.setPadding(new Insets(10));

        //Puts everything together
        HBox fullBoard = new HBox(sidePanel, boardWithButtons);

        primaryStage.setTitle("Connect 4");
        primaryStage.show();

        boardScene = new Scene(fullBoard);
        primaryStage.setScene(titleScene);

    }

    /**
     * Swaps the current turn (value and image)
     */
    private void swapTurn(){
        if(nextTurn == Color.RED) {
            nextTurn = Color.BLACK;
            turn.setImage(BLACK);
            return;
        }
        if(nextTurn == Color.BLACK){
            nextTurn = Color.RED;
            turn.setImage(RED);
        }
    }

    /**
     * Retrieves the image corresponding to the color
     * @param c Color
     * @return Image corresponding to color
     */
    private Image getImage(Color c){
        if(c == Color.RED) return RED;
        if(c == Color.BLACK) return BLACK;
        if(c == Color.EMPTY) return EMPTY;
        return null;
    }

    /**
     * Checks for a win at a position
     * @param i int row
     * @param j int column
     */
    private void checkWin(int i, int j){
        if(boardColor[i][j] == Color.EMPTY) return;
        if(winner) return;
        if(checkDown(i,j) || checkRight(i,j) || checkDownLeft(i,j) || checkDownRight(i,j) ||
           checkUp(i,j) || checkLeft(i,j) || checkUpLeft(i,j) || checkUpRight(i,j)){
            if(boardColor[i][j] == Color.RED){
                System.out.println("The winner is red.");
            }
            if(boardColor[i][j] == Color.BLACK) {
                System.out.println("The winner is black.");
            }
            winner = true;
        }
    }

    /**
     * Checks for win at all values on board
     */
    private void trueCheck(){

        boolean emptyExists = false;
        for(int i=0; i<ROWS; i++){
            for(int j=0; j<COLUMNS; j++){
                checkWin(i,j);
                if(boardColor[i][j] == Color.EMPTY) emptyExists = true;
                if(winner) return;
            }
        }
        if(!emptyExists) System.out.println("Draw");

    }

    /**
     * Checks for a win down from position
     * @param row int
     * @param column int
     * @return boolean returns true if win
     */
    private boolean checkDown(int row, int column){
        if(row < 0 || row > ROWS-4 || column < 0 || column >= COLUMNS) return false;
        Color central = boardColor[row][column];
        for(int i=1; i<4; i++){
            if(central != boardColor[row+i][column]) return false;
        }
        return true;
    }

    /**
     * Checks for a win right from position
     * @param row int
     * @param column int
     * @return boolean returns true if win
     */
    private boolean checkRight(int row, int column){
        if(column < 0 || column > COLUMNS-4 || row < 0 || row >= ROWS) return false;
        Color central = boardColor[row][column];
        for(int i=1; i<4; i++){
            if(central != boardColor[row][column+i]) return false;
        }
        return true;
    }

    /**
     * Checks for a win down-right from position
     * @param row int
     * @param column int
     * @return boolean returns true if win
     */
    private boolean checkDownRight(int row, int column){
        if(column < 0 || column > COLUMNS-4 || row < 0 || row > ROWS-4) return false;
        Color central = boardColor[row][column];
        for(int i=1; i<4; i++){
            if(central != boardColor[row+i][column+i]) return false;
        }
        return true;
    }

    /**
     * Checks for a win down-left from position
     * @param row int
     * @param column int
     * @return boolean returns true if win
     */
    private boolean checkDownLeft(int row, int column){
        if(column < 3 || column >= COLUMNS || row < 0 || row > ROWS-4) return false;
        Color central = boardColor[row][column];
        for(int i=0; i<4; i++){
            if(central != boardColor[row+i][column-i]) return false;
        }
        return true;
    }

    /**
     * Checks for a win up from position
     * @param row int
     * @param column int
     * @return boolean returns true if win
     */
    private boolean checkUp(int row, int column){
        if(row <= ROWS - 4 || row >= ROWS || column < 0 || column >= COLUMNS) return false;
        Color central = boardColor[row][column];
        for(int i=0; i<4; i++){
            if(central != boardColor[row-i][column]) return false;
        }
        return true;
    }

    /**
     * Checks for a win left from position
     * @param row int
     * @param column int
     * @return boolean returns true if win
     */
    private boolean checkLeft(int row, int column){
        if(column < COLUMNS - 4 || column >= COLUMNS || row < 0 || row >= ROWS) return false;
        Color central = boardColor[row][column];
        for(int i=0; i<4; i++){
            if(central != boardColor[row][column-i]) return false;
        }
        return true;
    }

    /**
     * Checks for a win up-left from position
     * @param row int
     * @param column int
     * @return boolean returns true if win
     */
    private boolean checkUpLeft(int row, int column){
        if(row <= ROWS-4 || row >= ROWS || column < COLUMNS-4 || column >= COLUMNS) return false;
        Color central = boardColor[row][column];
        for(int i=0; i<4; i++){
            if(central != boardColor[row-i][column-i]) return false;
        }
        return true;
    }

    /**
     * Checks for a win up-right from position
     * @param row int
     * @param column int
     * @return boolean returns true if win
     */
    private boolean checkUpRight(int row, int column){
        if(row <= ROWS-4 || row >= ROWS || column < 0 || column > COLUMNS - 4) return false;
        Color central = boardColor[row][column];
        for(int i=0; i<4; i++){
            if(central != boardColor[row-i][column+i]) return false;
        }
        return true;
    }

    /**
     * Main
     * @param args String[]
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Enum for Color types
     */
    enum Color{
        RED,
        BLACK,
        EMPTY
    }

    /**
     * Class to handle if column 0 button is pressed
     */
    class Column0Handler implements EventHandler<ActionEvent>{
        final int C = 0;

        /**
         * Called when button is pressed, updates column, checks for turn, and checks for win
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            if(winner || boardColor[0][C] != Color.EMPTY) return;
            for(int i=0; i<ROWS-1; i++){
                if(boardColor[i+1][C] != Color.EMPTY){
                    boardColor[i][C] = nextTurn;
                    boardImage[i][C].setImage(getImage(nextTurn));
                    swapTurn();
                    trueCheck();
                    return;
                }
            }
            boardColor[5][C] = nextTurn;
            boardImage[5][C].setImage(getImage(nextTurn));
            trueCheck();
            swapTurn();
        }
    }

    /**
     * Class to handle if column 1 button is pressed
     */
    class Column1Handler implements EventHandler<ActionEvent>{
        final int C = 1;

        /**
         * Called when button is pressed, updates column, checks for turn, and checks for win
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            if(winner || boardColor[0][C] != Color.EMPTY) return;
            for(int i=0; i<ROWS-1; i++){
                if(boardColor[i+1][C] != Color.EMPTY){
                    boardColor[i][C] = nextTurn;
                    boardImage[i][C].setImage(getImage(nextTurn));
                    swapTurn();
                    trueCheck();
                    return;
                }
            }
            boardColor[5][C] = nextTurn;
            boardImage[5][C].setImage(getImage(nextTurn));
            trueCheck();
            swapTurn();
        }
    }

    /**
     * Class to handle if column 2 button is pressed
     */
    class Column2Handler implements EventHandler<ActionEvent>{
        final int C = 2;

        /**
         * Called when button is pressed, updates column, checks for turn, and checks for win
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            if(winner || boardColor[0][C] != Color.EMPTY) return;
            for(int i=0; i<ROWS-1; i++){
                if(boardColor[i+1][C] != Color.EMPTY){
                    boardColor[i][C] = nextTurn;
                    boardImage[i][C].setImage(getImage(nextTurn));
                    swapTurn();
                    trueCheck();
                    return;
                }
            }
            boardColor[5][C] = nextTurn;
            boardImage[5][C].setImage(getImage(nextTurn));
            trueCheck();
            swapTurn();
        }
    }

    /**
     * Class to handle if column 3 button is pressed
     */
    class Column3Handler implements EventHandler<ActionEvent>{
        final int C = 3;

        /**
         * Called when button is pressed, updates column, checks for turn, and checks for win
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            if(winner || boardColor[0][C] != Color.EMPTY) return;
            for(int i=0; i<ROWS-1; i++){
                if(boardColor[i+1][C] != Color.EMPTY){
                    boardColor[i][C] = nextTurn;
                    boardImage[i][C].setImage(getImage(nextTurn));
                    swapTurn();
                    trueCheck();
                    return;
                }
            }
            boardColor[5][C] = nextTurn;
            boardImage[5][C].setImage(getImage(nextTurn));
            trueCheck();
            swapTurn();
        }
    }

    /**
     * Class to handle if column 4 button is pressed
     */
    class Column4Handler implements EventHandler<ActionEvent>{
        final int C = 4;

        /**
         * Called when button is pressed, updates column, checks for turn, and checks for win
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            if(winner || boardColor[0][C] != Color.EMPTY) return;
            for(int i=0; i<ROWS-1; i++){
                if(boardColor[i+1][C] != Color.EMPTY){
                    boardColor[i][C] = nextTurn;
                    boardImage[i][C].setImage(getImage(nextTurn));
                    swapTurn();
                    trueCheck();
                    return;
                }
            }
            boardColor[5][C] = nextTurn;
            boardImage[5][C].setImage(getImage(nextTurn));
            trueCheck();
            swapTurn();
        }
    }

    /**
     * Class to handle if column 5 button is pressed
     */
    class Column5Handler implements EventHandler<ActionEvent>{
        final int C = 5;

        /**
         * Called when button is pressed, updates column, checks for turn, and checks for win
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            if(winner || boardColor[0][C] != Color.EMPTY) return;
            for(int i=0; i<ROWS-1; i++){
                if(boardColor[i+1][C] != Color.EMPTY){
                    boardColor[i][C] = nextTurn;
                    boardImage[i][C].setImage(getImage(nextTurn));
                    swapTurn();
                    trueCheck();
                    return;
                }
            }
            boardColor[5][C] = nextTurn;
            boardImage[5][C].setImage(getImage(nextTurn));
            trueCheck();
            swapTurn();
        }
    }

    /**
     * Class to handle if column 6 button is pressed
     */
    class Column6Handler implements EventHandler<ActionEvent>{
        final int C = 6;

        /**
         * Called when button is pressed, updates column, checks for turn, and checks for win
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            if(winner || boardColor[0][C] != Color.EMPTY) return;
            for(int i=0; i<ROWS-1; i++){
                if(boardColor[i+1][C] != Color.EMPTY){
                    boardColor[i][C] = nextTurn;
                    boardImage[i][C].setImage(getImage(nextTurn));
                    swapTurn();
                    trueCheck();
                    return;
                }
            }
            boardColor[5][C] = nextTurn;
            boardImage[5][C].setImage(getImage(nextTurn));
            trueCheck();
            swapTurn();
        }
    }

    /**
     * Class to handle if reset button is pressed
     */
    class ResetHandler implements EventHandler<ActionEvent>{
        /**
         * Called when button is pressed, resets board values
         * @param event ActionEvent
         */
        @Override
        public void handle(ActionEvent event) {
            for(int i=0; i<ROWS; i++){
                for(int j=0; j<COLUMNS; j++){
                    boardColor[i][j] = Color.EMPTY;
                    boardImage[i][j].setImage(EMPTY);
                }
            }
            current.setText("Current");
            nextTurn = Color.BLACK;
            swapTurn();
            winner = false;
            trueStage.setScene(titleScene);
        }
    }

    class StartHandler implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent event) {
            trueStage.setScene(boardScene);
        }
    }

}